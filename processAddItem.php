<?php
// the pourpose of this module is to insert a new devotion into the devotion_table
// this module accepts the input from the showAddForm.php page.
// show a success message with a link to the index.php page

session_start();

require_once 'db_connector.php';

$badWords= array ("darnit", "idiot", "poop");

$devotionTopic= $_GET['devotionTopic'];
$devotionTitle= $_GET['devotionTitle'];
$devotionBody= $_GET['devotionBody'];
$user_id= $_SESSION['userid'];

// The if statement below will not allow the user to create a devotion if it has $badWords in the body.

$bodyContent = explode(" ", $devotionBody);
foreach ($bodyContent as $bodyWord) {
    foreach ($badWords as $badWord) {
        if (strtolower($bodyWord)==$badWord) {
            die ("No bad words");
        }
    }
    
}

$sql_statement= "INSERT INTO `devotion_table` (`id`, `devotion_topic`, `devotion_title`, `devotion_body`, `users_table_id`) VALUES (NULL, '$devotionTopic', '$devotionTitle', '$devotionBody', '$user_id');";


if ($connection) {
    $result= mysqli_query($connection, $sql_statement);
    if($result) {
        echo "Data inserted successfully!";
        echo "click <a href='index.php'>here</a> to return";
        
    }
    else {
        echo "Error in the sql " . mysqli_error($connection);
    }
}
else {
    echo "Error connecting " . mysqli_connect_error();
}

?>