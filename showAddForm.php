<!-- 
 this is a partial page
 purpose is to show the "Add Devotion" form
 even though the file extension is .php, all of the code is html.
 -->
 
<html>
<head>
<link rel="stylesheet" type= "text/css" href="myWelcomeDesign.css">
</head>
<body>
<div class="row">
<div class="leftcolumn">
<div class="card">


 <h2>ADD A DEVOTION</h2>
<h5>(Fill out all fields and search)</h5>
 
 	<div class="fakeimg" style="height:200px;">
		<form action="processAddItem.php">
 				<textarea rows="1" cols="50" name="devotionTopic" placeholder="Devotion Topic"></textarea><br>
 				<textarea rows="5" cols="50" name="devotionTitle" placeholder="Devotion Title"></textarea><br>
 				<textarea rows="5" cols="50" name="devotionBody" placeholder="Devotion Body"></textarea><br>
 				<button type="submit">Add</button>
 		</form>
 	</div>
 	
</div>
</div>
</div>
</body>
</html>