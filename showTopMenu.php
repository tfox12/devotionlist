<!-- 
This is a partial page. The purpose is to (1) Start the html page (2) set the CSS style (3) show the app title
(4) show the navigation menu.
-->

<html>
<title>Devotion List</title>
<head>
<link rel="stylesheet" type="text/css" href="myWelcomeDesign.css">
</head>
</html>
<body>
<img src="Bible.jpg" alt="logo"
style="float:right;width:42px;height:42px;">
<h1 style="text-align:center">Daily Devotion</h1>

<div class="header">
	<?php if (!isset($_SESSION['username'])):?>
	<span class="menu-item"><a href="login.html"></a></span>
	
	<?php else: ?>
	<span class="menu-item">Welcome <?php echo $_SESSION['username']; ?> <a href="logout.php">Logout</a></span>
	<?php endif; ?>
	
	| <span class="menu-item"><a href="index.php?pageNumber=1">Search</a> </span> | <span class="menu-item"><a href="index.php?pageNumber=2">Add New Devotion</a> </span> | <span class="menu-item"><a href="index.php?pageNumber=3">Home</a> </span>
	<?php if ( $_SESSION['role'] == 'admin'):?>
	| <span class="menu-item"><a href="showAdminPage.php">Admin</a> </span> 
	<?php endif; ?>
</div>




</body>